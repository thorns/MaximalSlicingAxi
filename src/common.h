#ifndef MS_COMMON_H
#define MS_COMMON_H

#define HAVE_OPENCL 0
#define MS_VERIFY  0
#define MS_POLAR   0
#define MS_CCZ4    0

#define SQR(x) ((x) * (x))
#define SGN(x) ((x) >= 0.0 ? 1.0 : -1.0)
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MIN(x, y) ((x) > (y) ? (y) : (x))
#define ARRAY_ELEMS(arr) (sizeof(arr) / sizeof(*arr))

/*
 * small number to avoid r=0 singularities
 */
#define EPS 1E-08

#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
static inline int64_t gettime(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_usec;
}

#endif /* MS_COMMON_H */
