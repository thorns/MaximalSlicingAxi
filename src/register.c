
#include "cctk.h"
#include "cctk_Arguments.h"
#include "Slicing.h"

int maximal_slicing_axi_register(void)
{
    Einstein_RegisterSlicing("maximal_axi");
    return 0;
}

void maximal_slicing_axi_register_mol(CCTK_ARGUMENTS)
{
    MoLRegisterConstrained(CCTK_VarIndex("ADMBase::alp"));
    MoLRegisterConstrained(CCTK_VarIndex("MaximalSlicingAxi::alpha_coeffs"));

    MoLRegisterSaveAndRestoreGroup(CCTK_GroupIndex("ADMBase::metric"));
    MoLRegisterSaveAndRestoreGroup(CCTK_GroupIndex("ADMBase::curv"));
}
