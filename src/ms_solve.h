/*
 * Maximal slicing -- actual solver code
 * Copyright (C) 2016 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MS_SOLVE_H
#define MS_SOLVE_H

#include "common.h"

#include "cctk.h"

#include "basis.h"

typedef struct MSSolverPriv MSSolverPriv;

typedef struct MSSolver {
    MSSolverPriv *priv;

    const BasisSet *basis[2];

    int nb_coeffs[2];
    int nb_colloc_points[2];

    double *coeffs;
} MSSolver;

int ms_solver_init(MSSolver **ctx,
                   cGH *cctkGH,
                   int basis_order_r, int basis_order_z,
                   double outer_bound, double filter_power, double input_filter_power);

void ms_solver_free(MSSolver **ctx);

int ms_solver_solve(MSSolver *ctx);

void ms_solver_print_stats(MSSolver *ctx);

#endif /* MS_SOLVE_H */
